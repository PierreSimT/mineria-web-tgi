## Aplicación Web de Minería de Datos en la Web
Esta aplicación web es bastante simple, simplemente recoge tweets y permite clasificarlos.

## Tecnologías Usadas
- [Django](https://www.djangoproject.com/)
- [Pattern](https://www.clips.uantwerpen.be/pattern)
- [Docker](https://www.docker.com/)

## Instalación
### Prerrequisitos
- Docker
- Docker-Compose

Como instalar los prerrequisitos [aqui](https://docs.docker.com/).

### Guía Paso a Paso
1. Descargamos este repositorio, `git clone https://gitlab.com/PierreSimT/mineria-web-tgi`
2. Nos encontramos dentro de la carpeta y ejecutamos `docker-compose up` en la terminal
3. Ejecutar `docker exec -it NOMBRE_CONTENEDOR_WEB bash` y nos encontraremos dentro del contenedor. Dentro del contenedor ejecutamos `python manage.py migrate`
4. Si todo se ha ejecutado correctamente vamos a la página http://localhost:8000/ y deberíamos de ver la aplicación.
