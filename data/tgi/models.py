import datetime

from django.db import models
from django.utils import timezone

# Create your models here.

class SearchModel(models.Model):
	search_text = models.CharField(max_length=200)
	date_search = models.DateTimeField('date searched')

class Tweet(models.Model):
	text = models.CharField(max_length=800)
	author = models.CharField(max_length=20)
	pub_date = models.DateTimeField('date published')
	classif = models.CharField(max_length=5)
	search = models.ForeignKey(SearchModel, on_delete=models.CASCADE)
	
class Hashtag(models.Model):
	hashtag_text = models.CharField(max_length=200)
	tweets = models.ManyToManyField(Tweet)
	
	def __str__(self):
		return self.hashtag_text