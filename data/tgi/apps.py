from django.apps import AppConfig


class TgiConfig(AppConfig):
    name = 'tgi'
