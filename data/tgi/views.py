from __future__ import print_function
from __future__ import unicode_literals

from builtins import str, bytes, dict, int
from builtins import range

import os
import sys
sys.path.insert(0, os.path.join(os.path.dirname(__file__), "..", ".."))

from pattern.web import Twitter, hashtags
from pattern.db import Datasheet, pprint, pd
from pattern.search import search
from pattern.vector import Document, Model, KNN
from pattern.en import Sentence, parse

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils import timezone

from .models import SearchModel, Tweet, Hashtag
import datetime

# Create your views here.

def index(request):
	return render(request, 'tgi/index.html')

def clasif(request):
	searches = SearchModel.objects.all()
	return render(request, 'tgi/clasif.html', {'searches': searches})
	
def classification(request):
	try:
		selected_search = SearchModel.objects.get(pk=request.POST['choice'])
	except (KeyError, SearchModel.DoesNotExist):
		return render(request, 'tgi/clasif.html', {
			'error_message': "No se ha elegido ninguna busqueda",
		})
	else:
		
		try:
			classifier = KNN.load(os.path.join(os.path.dirname(__file__), "classifier"))
		except:
			return render(request, 'tgi/clasif.html', {
				'error_message': "El clasificador no ha sido entrenado",
			})
		else:
			for tweet in selected_search.tweet_set.all():
				tweet.classif = classifier.classify(tweet.text)
				if tweet.classif == None:
					tweet.classif = "NaN"
				tweet.save()
			
			return render(request, 'tgi/classification.html', {'search': selected_search, 'classifier': classifier})
		
def detail(request):
	try:		
		classifier = KNN.load(os.path.join(os.path.dirname(__file__),  "classifier"))
	except:
		return render(request, 'tgi/detail.html', {
				'error_message': "El clasificador no existe",
				'classifier': classifier,
				'size': str(len(classifier.features)),
			})
	else:
		return render(request, 'tgi/detail.html', {'classifier': classifier, 'size': len(classifier.features)})

def search(request):

	try:
		newSearch = SearchModel.objects.get(search_text=request.POST['search'])
	except:
		newSearch = SearchModel(search_text=request.POST['search'], date_search=timezone.now())		
	else:
		newSearch.date_search = timezone.now()
		newSearch.tweet_set.all().delete()
	
	newSearch.save()
		
	engine = Twitter(language="en")
	prev = None
	index = set()
	
	for i in range(2):
		for tweet in engine.search(request.POST['search'], start=prev, count=10, cached=False):
			if tweet.id not in index:
				date = datetime.datetime.strptime(tweet.date, '%a %b %d %H:%M:%S %z %Y').strftime('%Y-%m-%d %H:%M')
				
				newTweet = newSearch.tweet_set.create(text=tweet.text, author=tweet.author, pub_date=date)
				
				hashes = hashtags(tweet.text)
				for hashtag in hashes:
					try:
						newHashtag = Hashtag.objects.get(hashtag_text=hashtag)
					except:
						newHashtag = Hashtag(hashtag_text=hashtag)
						newHashtag.save()
					
					newHashtag.tweets.add(newTweet)
				
				index.add(tweet.id)
				
			prev = tweet.id
	
	return HttpResponseRedirect(reverse('tgi:result', args=[newSearch.id]))
	
def result(request, search_id):
	search = get_object_or_404(SearchModel, pk=search_id)
	return render(request, 'tgi/result.html', {'search': search})
	
