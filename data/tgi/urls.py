from django.urls import path

from . import views

app_name = 'tgi'
urlpatterns = [
    path('', views.index, name='index'),
	path('search/', views.search, name='search'),
	path('detail', views.detail, name='detail'),
	path('clasif/', views.clasif, name='clasif'),
	path('classification/', views.classification, name='classification'),
	path('search/<int:search_id>', views.result, name='result'),
]