# Generated by Django 2.1.4 on 2018-12-31 17:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tgi', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tweet',
            name='text',
            field=models.CharField(max_length=500),
        ),
    ]
